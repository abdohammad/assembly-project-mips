
	# Program 2:
	# This program takes a single number from the standar input
	# and returns the number of digits the number contains,
	# for both positive and negative.
	# Author: Abdelrahman Hammad    Student ID: 112394561
	# Section: 0104                 TA: Mathew
	# Directory ID: ahammad

	.data
n:	.word 0
	.text

main:	li $v0 5	#Read value for n
	syscall
	move $t0, $v0	
	sw $t0, n	#Store the value in data segment
	
	li $sp, 0x7ffffffc	#Set the stack pointer
	sw $t0, ($sp)		#store parameter of function
	sub $sp, $sp, 4		#push stack pointer down

	jal f			#Jump to function
	
	add $sp, $sp, 4		#Pop argument from stack
	sw $v0, n		#Store returned value 
	
	li $v0, 1		#Printing value of n
	lw $a0, n
	syscall

	li $v0, 11		#Printing newline
	li $a0,10		
	syscall

	li $v0, 10		#Exiting program
	syscall


f:	sub $sp, $sp, 12	#Pushing the stack pointer down
	sw  $ra, 12($sp)	#Setting return address
	sw  $fp, 8($sp)		#Setting frame pointer
	add $fp, $sp, 12	#Pushing frame pointer to function frame

	li $t1, 0
	sw $t1, 4($sp)		#Setting local variable to 0
	lw $t2, 4($fp)		#Loading argument
	bgezal $t2, do		#If argument is negative		
	neg $t2, $t2		#then negate

do:	div $t2, $t2, 10	#Divide argument by 10
	add $t1, $t1, 1		#add one to local variable
	sw $t1, 4($sp)		#Storing new local variable
	bgtz $t2, do		#If argument is still larger than 0 then loop
	
	move $v0, $t1		#Return copy of local variable
	
	lw $ra, 12($sp)		#Load return address
	lw $fp, 8($sp)		#Load frame pointer
	add $sp, $sp, 12	#push stack pointer back
	jr $ra			#Return at return address
	
	
	
	
	
	
