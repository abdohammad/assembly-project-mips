Program 1:
        Program that reads three numbers. It prints the number of
	multiples in the third number that are between the
	first and seconds number inclusive. Result will always
	be 0 or greater.

Program 2:
	 This program takes a single number from the standard input
	 and returns the number of digits the number contains,
	 for both positive and negative.

Program 3:
	This program stores numbers fro standard input in an array
        until zero is read. Once zero is read it reads numbers from
	Standard input and searches for their indexes untill
	zero is read. If number is found in array then
	index is printed else -1 is printed. 