	#  Program 3:
	#   This program stores numbers fro standard input in an array
	# untill zero is read. Once zero is read it reads numbers from
	# Standard input and searches for their indexes untill
	# zero is read. If number is found in array then
	# index is printed else -1 is printed. 
	#  Author: Abdelrahman Hammad    Student ID: 112394561
	#  Section: 0104                 TA: Mathew
	#  Directory ID: ahammad 

	.data
arr:	.space 400 		#array of size 100
n:	.word 0			#Global variables arr, n, max
max:	.word 0			#element, idx, pos
element:	.word 0
idx:	.word 0
pos:	.word 0
	.text

main:	li $v0, 5		#Read value for n
	syscall
	move $t0, $v0
	sw $t0, n

while:	beqz $t0, eloop		#If n equals zero jump to end of loop
	lw $t2, max		#load max
	li $t3,100		#load 100
if:	bge $t2, $t3, eif	#If max is >/= then go to eif
	mul $t4, $t2, 4		#Multiply max by 4
	sw $t0, arr($t4)	#Store n in array with index as max
	add $t2, $t2, 1		#add one to max
	sw $t2, max		#Store new max
eif:	li $v0, 5		#Read new value for n
	syscall
	move $t0,$v0
	sw $t0,n		#Store n
	j while			#Jump to beginning of loop

eloop:li $v0, 5			#Read value for element
 	syscall
	move $t0, $v0		#Store value of element in data segment
	sw $t0, element

while2:	beqz $t0, endloop2	#If element = 0 then end loop
	li $t1, 0		#Load 0
	sw $t1, idx		#Store 0 in idx

while3: lw $t2, max		#Load max
	bge $t1, $t2, endloop3	#If idx >/= max then exit loop 
	mul $t4, $t1, 4		#Multiply index by 4
	lw $t5, arr($t4)	#Load word from array
	beq $t5, $t0, endloop3	#If arr[idx] != element then exit loop
	add $t1, $t1, 1  	#increasing idx
	sw $t1, idx		#Store new index
	j while3		#Jump to loop
	
endloop3:lw $t1, idx		#loading idx
	lw $t2, max		#loading max
	bge $t1, $t2, else	#If idx >/= max jump to else
	li $v0, 1		#Print idx
	move $a0, $t1		#Print t1
	syscall
	j newline

else:	li $v0, 1		#Printing -1
	li $a0, -1
	syscall

newline:li $v0, 11		#Printing newline
	li $a0, 10
	syscall

	li $v0, 5		#Read a new element
	syscall
	move $t0, $v0
	sw $t0, element
	j while2		

endloop2:	li $v0, 10	#End program
	 syscall
	
	
	
