
	#  Program 1:
	#  Program that reads three numbers. It prints the number of
	#  multiples in the third number that are between the
	#  first and seconds number inlusive. Result will always
	#  be 0 or greater.
	#  Author: Abdelrahman Hammad    Student ID: 112394561
	#  Section: 0104                 TA: Mathew
	#  Directory ID: ahammad  

	.data
m:	.word 0
n:	.word 0
mod_value:	.word 0
count:	.word 0

	.text

main:	li $v0, 5		#Read in m
	syscall	
	move $t0, $v0		#Retreive value read
	sw $t0,m		#Store m in register t0

	li $v0, 5		#Read in n
	syscall
	move $t1, $v0		#Retrieve value of n
	sw $t1,n		#Store value of n in register t1

	li $v0, 5		#Read in mod_value
	syscall		
	move $t2, $v0		#Retrieve value of mod_value
	sw $t2,mod_value	#Store value of mod_value in t2

loop:	bgt $t0,$t1,print	#If m > n jump to print label
	rem $t3, $t0, $t2	#Store remainder in register t3
	li $t4,0		#Store 0 in register t4
	bne $t3, $t4, eloop	#If remainder is not equal 0, jump to eloop
	
incr:	lw $t5, count		#load count into register t5
	li $t6, 1		#load 1 into register 6
	add $t5, $t5,$t6	#add count to 1
	sw $t5, count		#Store value of count
	
eloop:	li $t6, 1		#Store 1 in register t6
	add $t0, $t0, $t6	#Add 1 to m
	sw $t0, m		#store m in register t0
	j loop			#Go back to start of loop

print:	li $v0, 1		#Prepare syscall print
	lw $a0, count		#Place count in print register
	syscall			#Print

	li $v0, 11		#Prepare syscall print char
	li $a0, 10		#Place new line in print register 
	syscall			#Print

	li $v0, 10		#Exit
	syscall
